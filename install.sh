#!/bin/bash

######################################################################
#
# This script will install and configure all packages
# needed when makig new arch linux distro, and setup
# initial configuration that I use.
#
######################################################################
PACKAGES=(  xorg-xinit xorg-server i3-gaps         \
			feh terminator noto-fonts-emoji        \
			ttf-dejavu pavucontrol pulseaudio rofi \
			compton i3lock openssh i3status        \
			neofetch chromium python pip           \
			transmission-cli vlc )

DOTFILE_DIR="./dotfiles"
I3_CONFIGS="./i3config"
WALLPAPER="./wallpaper.jpg"

function print_help() {
	# Print help
	echo "Usage: $0 [-u|--user] <username>"
	echo		
	echo "	-u|--user: specify a user as a target of installation."
	echo
	echo
	echo "A script to install all necesities for the system to work as it does for me"
	echo "Needs to be run as a root user."
}

function throw_error {
	# Simple convenience error function
	echo "$@"
	exit 1
}

function is_user_valid() {
	# Check user existence and if specified at all
	[ "$1" != "" ] && id -u "$1" 2>&1 >/dev/null
	return "$?"
}

function move_dotfiles() {
	# Move all dotfiles to destination folder specified in $1
	for f in $(ls "$DOTFILE_DIR"); do
		cp "$DOTFILE_DIR/$f" "$1/.$f"
	done
}

function move_i3files() {
	for f in $(ls "$I3_CONFIGS"); do
		cp "$I3_CONFIGS/$f" "$1/$f"
	done
}

function move_system_files() {
	mkdir -p /etc/X11/xorg.conf.d
	cp ./90-touchpad.conf /etc/X11/xorg.conf.d/90-touchpad.conf
	cp ./backlight_service.service /etc/systemd/system/backlight_rights.service
	echo "#!/bin/bash" > /root/backlight.sh
	echo "chgrp wheel /sys/class/backlight/intel_backlight/brightness && chmod 664 /sys/class/backlight/intel_backlight/brightness" >> /root/backlight.sh
	chmod +x /root/backlight.sh
	systemctl enable backlight_rights.service
}

function main() {
	# Parse input arguments

	for i in "$@"; do
		case $i in 
			-h|--help)
				print_help
				exit 0
				;;
			-u|--user)
				shift
				TARGET_USER="$1"
				shift
				;;
			--default)
				DEFAULT=YES
				shift
				;;
			*)
				;;
		esac
	done

	# Check if running as root, which need's to be done
	! [ "$USER" == "root" ] && throw_error "This scripts is to be run as root."
	is_user_valid "$TARGET_USER" || throw_error "User was not set or not found."

	pacman -S "${PACKAGES[@]}"
	
	mkdir -p "/home/${TARGET_USER}/.config/i3"
	move_dotfiles "/home/${TARGET_USER}"
	move_i3files "/home/${TARGET_USER}/.config/i3"
	move_system_files

	mkdir -p /home/${TARGET_USER}/.config/ranger
	cp "${DOTFILES}/rc.conf" "/home/${TARGET_USER}/.config/ranger/rc.conf"

	# Move wallpaper for feh to load properly
	mv "$WALLPAPER" "/home/${TARGET_USER}/.wallpaper.jpg"

	echo "All went well. Reboot for changes to take full effect."
}

main "$@"

