#!/usr/bin/python

from youtube_search import YoutubeSearch as ys
import time
import argparse
from subprocess import Popen, PIPE, DEVNULL
import random
import sys

MEDIA_PLAYER='cvlc'

parser = argparse.ArgumentParser(description='A short script to search for and play youtube videos from terminal.')
parser.add_argument('-c', '--count', type=int, default=1, help='Count of results to search.')
parser.add_argument('-t', '--top', type=int, default=-1, help='Top n results to consider. Default is equal to count.')
parser.add_argument('-s', '--shuffle', action='store_true', help='If set, results are shuffled.')
parser.add_argument('-p', '--player', type=str, default='vlc', help='Specify a command to use as a media player.')
parser.add_argument('search_term', nargs='+', help='Term to search for on youtube.')

def main():
	args = parser.parse_args()

	# Check constraint
	if args.top != -1 and args.count > args.top:
		args.top == args.count
		print('Top results needs to be >= than count.')
		print('Aborting...')
		sys.exit(1)

	results = ys('+'.join(args.search_term), max_results=args.top if args.top != -1 else args.count).to_dict()

	if len(results) == 0:
		print('Youtube declined request.. attempting again')
		time.sleep(1)
		main()
		return
	
	base_link = 'https://www.youtube.com'
	videos = [ (r['title'], base_link+r['link']) for r in results ]

	if args.shuffle == True:
		random.shuffle(videos)

	print(*[v[0] for v in videos[:args.count]], sep='\n')

	process = Popen(
		[args.player] + [v[1] for v in videos[:args.count]],
		stdout=DEVNULL,
		stderr=DEVNULL
	)

if __name__ == '__main__':
	main()
