#!/bin/bash

df -h / | sed '2!d;q' | awk -F' ' '{print $4}'
