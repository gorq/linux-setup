#!/bin/bash

used=$(free -m | sed '2!d;q' | awk -F' ' '{print $3}')
res=$(bc -l <<< "scale=1; $used/1000")
echo ${res}G

